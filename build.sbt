val scala3Version = "2.13.6"

lazy val root = project
  .in(file("."))
  .settings(
    name := "url-shortener",
    version := "0.1.0",

    scalaVersion := scala3Version,

    libraryDependencies ++= Seq(
      "org.typelevel" %% "cats-effect" % "2.5.1",
      "co.fs2" %% "fs2-core" % "2.5.9",
      "org.http4s" %% "http4s-dsl" % "0.21.24",
      "org.http4s" %% "http4s-blaze-server" % "0.21.24",
      "org.apache.logging.log4j" % "log4j-api"  % "2.14.1",
      "org.apache.logging.log4j" % "log4j-core" % "2.14.1",
      "org.apache.logging.log4j" % "log4j-slf4j-impl" % "2.14.1",
      "org.scalatest" %% "scalatest" % "3.2.9" % Test,
    )
  )

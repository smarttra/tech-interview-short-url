## URL Shortener Service Interview Task

Implement a service that shortens urls. A User can shorten a valid URL to produce a short URL. The User can also retrieve 
their original URL using the short URL. Aim to generate the shortest URL code possible.

Short URLs must be generated uniquely across multiple running service instances. After a service restart, existing short URLs must be retrievable.

The solution must:

* be implemented using the Typesafe stack (cats-effect, http4s, fs2.Stream) in Scala 2.13 (or 3.0)
* be implemented using the Test-Driven-Development approach with passing tests (write tests first)
* implement an endpoint that accepts a valid URL as a parameter and responds with the short url
* implement an endpoint that accepts a short URL as a parameter and responds with the original url
* generate short url codes for a given url
* implement validation and failure handling
* generate short urls uniquely when multiple service instances are running
* be your own work

An example test case UrlShortenerServiceSpec is provided. You may rewrite this for a different scalatest style
if you wish. All the components should be covered by unit tests. As an extra goal, you may implement
integration test(s) using the testcontainers-scala library.

The development should take about ~4 hours. The solution should be submitted within 7 days.
